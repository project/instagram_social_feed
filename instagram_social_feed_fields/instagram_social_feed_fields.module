<?php
/**
 * @file
 * Hook implementations for the instagram_social_feed_fields module.
 *
 * This sub-module started as a copy of the similar sub-module of Drupagram
 * created by Damien McKenna.
 */

/**
 * Field definition.
 */

/**
 * Implements hook_field_info().
 */
function instagram_social_feed_fields_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'instagram_social_feed_last_pic' => array(
      'label' => t('Instagram Social Feed: Last photo'),
      'description' => t('Display the last photo for a specific Instagram feed.'),
      'default_widget' => 'instagram_social_feed_last_pic_widget',
      'default_formatter' => 'instagram_social_feed_last_pic_formatter',
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function instagram_social_feed_fields_field_is_empty($item, $field) {
  return empty($item['feed_id']);
}

/**
 * Field widget.
 */

/**
 * Implements hook_field_widget_info().
 */
function instagram_social_feed_fields_field_widget_info() {
  return array(
    'instagram_social_feed_last_pic_widget' => array(
      'label' => t('Instagram feed selector'),
      'field types' => array('instagram_social_feed_last_pic'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function instagram_social_feed_fields_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $main_widget = $element;
  $main_widget['#delta'] = $delta;
  $empty_widget = array();

  if ($instance['widget']['type'] == 'instagram_social_feed_last_pic_widget') {
    global $user;
    $main_widget += array(
      '#type' => 'select',
      '#options' => instagram_social_feed_fields_get_all_feeds(),
      '#default_value' => isset($items[$delta]['feed_id']) ? $items[$delta]['feed_id'] : '',
      '#required' => TRUE,
    );
    if (!empty($main_widget['#description'])) {
      $main_widget['#description'] .= ' ';
    }
    $main_widget['#description'] .= t('Need to <a href="@url">add another feed</a> to the list?', array('@url' => url('admin/config/services/instagram_social_feed')));
    $empty_widget = array(
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]['empty_message']) ? $items[$delta]['empty_message'] : '',
      '#title' => t('Message to use if no photos found'),
    );
  }

  $element['feed_id'] = $main_widget;
  $element['empty_message'] = $empty_widget;

  return $element;
}

/**
 * Field formatter.
 */

/**
 * Implements hook_field_formatter_info().
 */
function instagram_social_feed_fields_field_formatter_info() {
  return array(
    // Display the picture.
    'instagram_social_feed_last_pic_formatter' => array(
      'label' => t('Standard formatter'),
      'field types' => array('instagram_social_feed_last_pic'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function instagram_social_feed_fields_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if ($display['type'] == 'instagram_social_feed_last_pic_formatter') {
    module_load_include('inc', 'instagram_social_feed');

    foreach ($items as $delta => $item) {
      // Load the most recent info for this feed.
      $result = db_query("SELECT *
        FROM {instagram_social_feed_photos}
        WHERE feed_id = :feed_id
        ORDER BY created_time DESC
        LIMIT 1",
        array(':feed_id' => $item['feed_id']))
        ->fetchObject();

      if (!empty($result)) {
        // Build the field.
        $instagram_user = check_plain($result->instagram_user);
        $instagram_photo_url = check_url($result->instagram_link);
        $photo_url_parsed = parse_url($instagram_photo_url);
        $instagram_account_url = $photo_url_parsed['scheme'] . '://' . $photo_url_parsed['host'] . '/' . $instagram_user;
        $element[$delta] = array(
          '#theme' => 'instagram_social_feed_last_pic',
          '#instagram_user' => $instagram_user,
          '#instagram_account_url' => $instagram_account_url,
          '#cta_text_inline' => 'Follow @' . $instagram_user . ' on Instagram',
          '#cta_text_stacked' => 'Follow<br>@' . $instagram_user . '<br> on Instagram',
          '#instagram_photo_url' => $instagram_photo_url,
          '#thumbnail_photo' => check_url($result->thumbnail),
          '#thumbnail_width' => check_plain($result->thumbnail_width),
          '#thumbnail_height' => check_plain($result->thumbnail_height),
          '#low_resolution_photo' => check_url($result->low_resolution),
          '#low_resolution_width' => check_plain($result->low_resolution_width),
          '#low_resolution_height' => check_plain($result->low_resolution_height),
          '#standard_resolution_photo' => check_url($result->standard_resolution),
          '#standard_resolution_width' => check_plain($result->standard_resolution_width),
          '#standard_resolution_height' => check_plain($result->standard_resolution_height),
          '#caption' => filter_xss($result->caption),
        );
      }
      elseif (!empty($item['empty_message'])) {
        $element = array(
          '#markup' => $item['empty_message'],
        );
      }
      else {
        watchdog('Instagram Social Feed', 'No photos found for Instagram feed ID: @id', array('@id' => $item['feed_id']));
      }
    }
  }

  return $element;
}

/**
 * Internal functions.
 */

/**
 * Get a list of all enabled feeds by ID and name.
 */
function instagram_social_feed_fields_get_all_feeds() {
  $feeds = array();

  $results = db_select('instagram_social_feeds', 's')
    ->fields('s', array())
    ->condition('enabled', 0,'>')
    ->execute();

  foreach ($results as $row) {
    $feeds[$row->id] = check_plain($row->name);
  }

  return $feeds;
}
