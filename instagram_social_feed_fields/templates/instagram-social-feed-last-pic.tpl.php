<?php
/**
 * @file
 * Default template file for Instagram Social Feed Fields data.
 */

/**
 * Available variables:
 * $instagram_user - The Instagram user ID text.
 * $instagram_account_url - The URL for the Instagram account page.
 * $cta_text_inline - Call to action text to follow user on Instagram.
 * $cta_text_stacked Call to action text with line breaks between words.
 * $instagram_photo_url - The URL for the photo's page on Instagram.
 * $thumbnail_photo - The URL for the thumbnail version of the photo.
 * $thumbnail_width - Width of the thumbnail photo.
 * $thumbnail_height - Height of the thumbnail photo.
 * $low_resolution_photo - The URL for the low res version of the photo.
 * $low_resolution_width - Width of the low res photo.
 * $low_resolution_height - Height of the low res photo.
 * $standard_resolution_photo - The URL for the normal res version of the photo.
 * $standard_resolution_width - Width of the normal res photo.
 * $standard_resolution_height - Height of the normal res photo.
 * $caption - The text for the caption of the photo on Instagram.
 */
?>

<?php
  // Add a class to help crop images that are not square.
  if (intval($standard_resolution_width) > intval($standard_resolution_height)) {
    $crop = 'landscape-crop';
  }
  elseif (intval($standard_resolution_width) < intval($standard_resolution_height)) {
    $crop = 'portrait-crop';
  }
  else {
    $crop = '';
  }
?>

<div class="landing-page-recent-instagram">
  <div class="field">
    <div class="instagram-media <?php print $crop; ?>">
      <a href="<?php print $instagram_account_url; ?>" target="_blank">
        <img typeof="foaf:Image" class="instagram-photo" src="<?php print $standard_resolution_photo; ?>" alt="<?php print $caption; ?>">
      </a>
    </div>
  </div>
  <div class="field">
    <div class="instagram-caption">
      <div><a href="<?php print $instagram_account_url; ?>" target="_blank"><?php print $cta_text_stacked; ?></a></div>
    </div>
  </div>
</div>
